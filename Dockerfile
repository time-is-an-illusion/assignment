# syntax=docker/dockerfile:1

################
# Stage 1 - Base
################
FROM circleci/python:3.9.6-bullseye-browsers as base
# Root user because I'm a loose cannon
USER root
# Setup source directory
COPY src ./src
# Install application dependencies
RUN pip install --no-cache-dir --user -r src/requirements.txt


#######################
# Stage 2 - Development
#######################
FROM base as development
# Flask environment variables
ENV FLASK_APP=src/app.py
ENV FLASK_ENV=development
ENV FLASK_DEBUG=0
# Expose port
EXPOSE 5000
# Run flask
CMD [ "python", "-m" , "flask", "run", "--host=0.0.0.0"]


###################
# Stage 3 - Testing
###################
FROM development as testing
# Install selenium
RUN curl -O http://selenium-release.storage.googleapis.com/3.9/selenium-server-standalone-3.9.1.jar
# Setup directories
COPY test ./test
# Expose port
EXPOSE 4444