from flask_wtf import FlaskForm
from wtforms import SubmitField, TextAreaField
from wtforms.validators import DataRequired, Length

# Flask bulletin post form specification
class PostForm(FlaskForm):
    post = TextAreaField("Write something to place on the bulletin board", validators=[
        DataRequired(), Length(min=1, max=140)]
    )
    submit = SubmitField("Submit")