from flask import Flask, app
from flask_bootstrap import Bootstrap

bootstrap = Bootstrap()

def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'SECRETKEYFORFLASKWTFCONFIGURATION'
    bootstrap.init_app(app)

    return app

app = create_app()