from flask import render_template, redirect, request, url_for, make_response
from .forms import PostForm
from . import app


# ====================POSTS====================


# List of posts
POSTS = []

USER_PREFIX = "ANON"
USER_COUNTER = 0


# Bare-bones post object
class Post:
    def __init__(self, user, message):
        self.user = user
        self.message = message


# Create new user id
def new_user():
    global USER_PREFIX, USER_COUNTER
    USER_COUNTER = USER_COUNTER + 1
    user = USER_PREFIX.upper() + str(USER_COUNTER).zfill(8)
    return user


# Create new post
def create_post(user, message):
    post = Post(user, message)
    POSTS.insert(0, post)


# Create seed data for posts list
def populate_posts():
    for ii in range(10):
        user = new_user()
        create_post(user, "Default Message")


populate_posts()


# ====================FLASK====================


# Index route
@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
def index():
    if 'user_id' not in request.cookies:
        user = new_user()
        response = make_response(
            redirect("http://localhost:5000/welcome?name=" + user))
        response.set_cookie('user_id', user)
        response.set_cookie('session_token', 'SUP3R^S3CR3T^S3SSION^TOK3N')
        return response
    return redirect("http://localhost:5000/welcome?name=" + request.cookies.get('user_id'))


# Welcome route
@app.route('/welcome', methods=['GET'])
def welcome():
    return render_template('welcome.html')


# Bulletin route
@app.route('/bulletin', methods=['GET', 'POST'])
def bulletin():
    form = PostForm()
    if form.validate_on_submit():
        create_post(request.cookies.get('user_id'), form.post.data)
        return redirect(url_for("bulletin"))
    return render_template('bulletin.html', form=form, posts=POSTS)