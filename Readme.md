# DOM-Based XSS

In this assignment, our group will be detailing and exhibiting DOM-Based XSS through a simple web app we have made via flask.

Group Members

-   Oliver Pearson (18352773)

-   Joshua Thomas (20202996)

-   Hudhaifa Bhorat (18473328)

-   Elizabeth Timperley (20198037)

## How To Run

Navigate to the branch you would like to review, then build and run the app.

    git checkout BRANCHNAME
    make build run

Once make completes, navigate to localhost:5000 in your browser; this is our web app.

> Ensure you make build when switching branches.

## How To Test

**Terminal 1:** Navigate to the branch you would like to test, then build and run the app.

    git checkout BRANCHNAME
    make build-testing
    make run-testing
    
**Terminal 2:** Run the tests, the output will be placed in a "test.log" file in the root project directory.

    make test

> Ensure you make build-testing when switching branches.

## Branches

-   master

    -   Contains both reflected and stored DOM XSS
        -   On the home page (http://localhost:5000/welcome?name=ANON00000011) we have reflected XSS.
        -   On the bulletin page (http://localhost:5000/bulletin) we have stored XSS.

-   patches
    -   This branch contains the same pages as master but has both types of XSS vulnerabilities patched so any XSS attacks will fail.

## Exploit Vulerabilites

Once you have built the make image within the "master" branch, head over to the web app.

### Home Page

> http://localhost:5000/welcome?name=ANON00000011

To exploit the reflected XSS here simply replace the current name within the web address with some kind of script, an example has been provided below.

    <script>alert("Reflected XSS Working")</script>

With the above example we will have the following address:

    http://localhost:5000/welcome?name=<script>alert("Reflected XSS Working")</script>

Navigating here will result in the attack being executed.

> The popup that appears is proof that you were able to escape get the
> DOM to write an HTML element that contains the script.

### Bulletin

> http://localhost:5000/bulletin

To exploit the stored XSS here. Enter a script into the input field and click submit. Another example is provided below.

    <script>alert("Stored XSS Working")</script>

> The popup that appears is proof that you were able to escape get the
> DOM to write a HTML element that contains the script.

## Contribution

| Member              | Contribution                                                                   | Section(s) Presented                                   |
| ------------------- | ------------------------------------------------------------------------------ | ------------------------------------------------------ |
| Oliver Pearson      | Google Slides, Presentation Script, Readme.adoc                                | Background, Vulnerability Definition                   |
| Joshua Thomas       | Core Application, Web App Design, Unit Tests, CircleCi CI/CD, Docker, Makefile | ~                                                      |
| Hudhaifa Bhorat     | Google Slides, Presentation Script                                             | Introduction, Conclusion, Impact                       |
| Elizabeth Timperley | Vulnerability Patches, CircleCi CI/CD                                          | Real-World Incidents, Detection, Prevention, Code Demo |
