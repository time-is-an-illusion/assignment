import unittest
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


class SeleniumTestCase(unittest.TestCase):
    client = None
    
    @classmethod
    def setUpClass(cls):
        # Start chrome
        try:
            cls.client = webdriver.Chrome(service_args=["--verbose"])
        except:
            pass

    @classmethod
    def tearDownClass(cls):
        if cls.client:
            # Stop chrome
            cls.client.close()

    def setUp(self):
        if not self.client:
            self.skipTest('Browser not available')

    def tearDown(self):
        pass

    def test_selenium_welcome_page(self):
        url = 'http://localhost:5000/'
        self.client.get(url)
        self.assertTrue('<h3>Vulnerability - Reflected DOM Based XSS</h3>' in self.client.page_source)

    def test_reflected_vulnerability_fixed(self):
        url = 'http://localhost:5000/welcome?name='
        payload = '<h1 id="exploited">EXPLOITED</h1>'
        
        # Navigate to welcome page
        self.client.get(url + payload)

        # Test vulnerability
        try:
            element = self.client.find_element_by_id('exploited')
            self.assertTrue(element == None)
        except NoSuchElementException:
            self.assertTrue(True)


    def test_stored_vulnerability_fixed(self):
        url = 'http://localhost:5000/bulletin'
        payload = '<h1 id="exploited">EXPLOITED</h1>'

        # Navigate to bulletin page
        self.client.get(url)

        # Place payload
        self.client.find_element_by_tag_name('textarea').send_keys(payload)
        self.client.find_element_by_name('submit').click()

        # Test vulnerability
        try:
            element = self.client.find_element_by_id('exploited')
            self.assertTrue(element == None)
        except NoSuchElementException:
            self.assertTrue(True)
