IMG1=isec3004.assignment:dev
IMG2=isec3004.assignment:test
IMAGES=$(IMG1) $(IMG2) 
NAME=FLASK-DOM-XSS
LOG=test.log

help:
	@echo Available Makefile Commands:
	@echo make build                ; build the dev environment Docker image
	@echo make run                  ; run the dev environment Docker image
	@echo make build-testing        ; build the test environment Docker image
	@echo make run-testing          ; run the test environment Docker image
	@echo make test                 ; test the running Docker test container
	@echo make clean                ; remove all unused images, containers, networks, and all build cache

all: build run

build:
	docker build --target development --rm -t $(IMG1) .
	docker image prune -f

run: build
	docker run -p 0.0.0.0:5000:5000/tcp -it --name $(NAME) --rm $(IMG1)

build-testing:
	docker build --target testing --rm -t $(IMG2) .
	docker image prune -f

run-testing: build-testing
	docker run -v ~/$(LOG):/$(LOG) -p 0.0.0.0:5000:5000/tcp -it --name $(NAME) --rm $(IMG2)

test:
	docker exec -d $(NAME) java -jar selenium-server-standalone-3.9.1.jar
	docker exec -it $(NAME) python -m unittest -v > ./$(LOG)

clean:
	docker image rm $(IMAGES)
	docker system prune

.PHONY: help all build run build-testing test clean
